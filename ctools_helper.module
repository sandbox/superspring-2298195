<?php

/**
 * @file
 * Helps out the ctools module when it is moved around the filesystem.
 */

/**
 * Implements hook_menu_site_status_alter().
 */
function ctools_helper_menu_site_status_alter(&$menu_site_status, $path) {
  // Run the commands here so it runs first out of almost everything.
  if (_ctools_helper_issue_exists()) {
    // There is an issue. Update the path in the database.
    ctools_helper_run_fix();
  }
}

/**
 * Run the database fixes.
 */
function ctools_helper_run_fix() {
  ctools_helper_update_path();
  ctools_helper_clear_caches();
}

/**
 * Is there an issue with ctools currently?
 *
 * Attempt to load one of the files.
 */
function _ctools_helper_issue_exists() {
  // Where is the ctools directory?
  $path = drupal_get_path('module', 'ctools');

  // Is it missing?
  if (_ctools_helper_missing_directory($path)) {
    return TRUE;
  }

  // Load the ctools plugin information.
  module_load_include('inc', 'ctools', 'includes/plugins');

  // Load the paths stored in the ctools cache.
  $ctools_plugin_info = ctools_plugin_get_plugin_type_info();
  $ctools_process_paths = array();
  foreach ($ctools_plugin_info['ctools'] as $cache_item) {
    if (is_array($cache_item['process']) && array_key_exists('path', $cache_item['process'])) {
      $ctools_process_paths[] = $cache_item['process']['path'];
    }
  }

  // Do these paths exist?
  $ctools_process_paths = array_unique($ctools_process_paths);
  foreach ($ctools_process_paths as $ctools_process_path) {
    if (_ctools_helper_missing_directory($ctools_process_path)) {
      // No?
      return TRUE;
    }
  }

  // Looks ok, no issue.
  return FALSE;
}

/**
 * Is a directory missing or not readable?
 *
 * @param string $path
 *   The path to check.
 *
 * @return boolean
 *   Whether the directory does not exists or is not readable.
 */
function _ctools_helper_missing_directory($path) {
  return !file_exists($path) || !is_dir($path) || !is_readable($path);
}

/**
 * Clear the individual caches which ctools uses.
 */
function ctools_helper_clear_caches() {
  // Clear database caches.
  cache_clear_all('ctools_plugin_type_info', 'cache');
  cache_clear_all('lookup_cache', 'cache_bootstrap');
  cache_clear_all('ctools_plugin_type_info', 'cache');

  // Then just clear every other cache for good luck.
  drupal_flush_all_caches();
}

/**
 * Update where the database thinks ctools is.
 */
function ctools_helper_update_path() {
  // Prepare variables.
  $path  = drupal_get_path('module', 'ctools');

  // Update the system table.
  foreach (_ctools_helper_get_system_file_list() as $module => $filename) {
    db_update('system')->fields(array(
       'filename' => $path . DIRECTORY_SEPARATOR . $filename,
    ))
    ->condition('name', $module)
    ->condition('type', 'module')
    ->execute();
  }

  // Update the registry table.
  foreach (_ctools_helepr_get_registry_file_list() as $module => $filename) {
    $r = db_update('registry')->fields(array(
      'filename' => $path . DIRECTORY_SEPARATOR . $filename,
    ))
    ->condition('name', $module)
    ->condition('module', 'ctools')
    ->execute();
  }
}

/**
 * Get a list of where the files should be.
 *
 * Returns a list of all the module files inside the ctools project.
 */
function _ctools_helper_get_system_file_list() {
  return array(
    'ctools_plugin_example' => 'ctools_plugin_example/ctools_plugin_example.module',
    'ctools'                => 'ctools.module',
    'ctools_export_test'    => 'tests/ctools_export_test/ctools_export_test.module',
    'ctools_access_ruleset' => 'ctools_access_ruleset/ctools_access_ruleset.module',
    'ctools_ajax_sample'    => 'ctools_ajax_sample/ctools_ajax_sample.module',
    'ctools_custom_content' => 'ctools_custom_content/ctools_custom_content.module',
    'ctools_plugin_test'    => 'tests/ctools_plugin_test.module',
  );
}

/**
 * Get a list of where the files should be.
 *
 * Returns a list of all the module files inside the ctools project.
 */
function _ctools_helepr_get_registry_file_list() {
  return array(
    'ctools_context'                  => 'includes/context.inc',
    'ctools_context_required'         => 'includes/context.inc',
    'ctools_context_optional'         => 'includes/context.inc',
    'ctools_math_expr'                => 'includes/math-expr.inc',
    'ctools_math_expr_stack'          => 'includes/math-expr.inc',
    'ctools_stylizer_image_processor' => 'includes/stylizer.inc',
    'ctools_export_ui'                => 'plugins/export_ui/ctools_export_ui.class.php',
  );
}
